#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

typedef struct s_elfe
{
    uint32_t elfe;
    uint32_t sum;
} ts_elfe;

ts_elfe mTabElfe[512];
size_t nbElfe;

ts_elfe mMaxCapa[3];

void doCompute(ts_elfe *parElfe)
{
    if (parElfe == NULL)
    {

    }
    else
    {
        if (mMaxCapa[0].sum < parElfe->sum)
        {
            mMaxCapa[2] = mMaxCapa[1];
            mMaxCapa[1] = mMaxCapa[0];
            mMaxCapa[0] = *parElfe;
        }
        else if (mMaxCapa[1].sum < parElfe->sum)
        {
            mMaxCapa[2] = mMaxCapa[1];
            mMaxCapa[1] = *parElfe;
        }        
        else if (mMaxCapa[2].sum < parElfe->sum)
        {
            mMaxCapa[2] = *parElfe;
        }
    }
}

int doProcessFile(FILE *input)
{
    int wRes = 0;

    char line[64];

    mTabElfe[wRes].elfe = wRes;
    mTabElfe[wRes].sum = 0;

    while (!feof(input))
    {
        char * r = fgets(line, sizeof(line), input);

        if ((line[0] != '\n') && !feof(input))
        {
            uint32_t cal;
            sscanf(line, "%" PRIu32, &cal);
            mTabElfe[wRes].sum += cal;
        }
        else
        {
            fprintf(stdout, "Efle %" PRIu32 " : %" PRIu32 "\n", mTabElfe[wRes].elfe, mTabElfe[wRes].sum );

            doCompute(&mTabElfe[wRes]);

            wRes++;
            mTabElfe[wRes].sum = 0;
            mTabElfe[wRes].elfe = wRes;
        }
    }

    return wRes;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        nbElfe = doProcessFile(input);

        fprintf(stdout, "NB Elfe : %" PRIu32 "\n", nbElfe);

        for (size_t i = 0; i < 3; i++)
        {
            fprintf(stdout, "Elfe %" PRIu32 " : %" PRIu32 "\n", mMaxCapa[i].elfe, mMaxCapa[i].sum);
            total += mMaxCapa[i].sum;
        }
        

        fprintf(stdout, "Max Cal Found : %" PRIu32 "\n", total);

        fclose(input);

    }

    return wres;
}
