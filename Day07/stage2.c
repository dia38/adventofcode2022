#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

#define NAME_SIZE 60

const size_t disk = 70000000;
const size_t need = 30000000;

typedef struct s_element
{
    char    name[NAME_SIZE];
    size_t  size;
    struct s_element *parent;
    struct s_element *dir;
    struct s_element *next;
} ts_element;

static ts_element f1 = {

};
static ts_element root = {
    .name   = "/",
    .dir    = &f1,
    .size   = 0,
    .next   = &root,
    .parent = NULL,
};

ts_element *gotToDir(char *name, ts_element *cur)
{
    ts_element *s = cur;
    assert(name != NULL);
    assert(cur != NULL);
 
    while ((cur->next != s) && (strcmp(name, cur->name) != 0))
    {
        cur = cur->next;
    }

    return cur;
}

size_t doCompute(ts_element *cur)
{
    ts_element *s = cur;
    size_t wRes = 0;

    do
    {
        if (cur->dir != NULL)
        {
            cur->size = doCompute(cur->dir);
            wRes += cur->size;
        }
        else
        {
            wRes += cur->size;
        }

        cur = cur->next;
    }
    while ((cur->next != s));
   
    return wRes;
}

ts_element *found = NULL;

size_t doComputeS1(ts_element *cur, size_t need)
{
    ts_element *s = cur;

    do
    {
        if (cur->dir != NULL)
        {
            size_t tmp = doComputeS1(cur->dir, need);
            if (cur->size < need)
            {
            }
            else if (found == NULL)
            {
                found = cur;
            }
            else if (found->size > cur->size)
            {
                found = cur;
            }
        }
 
        cur = cur->next;
    } 
    while  ((cur->next != s));
   
    if ( found != NULL)
    {
        return found->size;
    }
    else
    {
        return 0;
    }
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t wStage1 = 0;
    char line[128];
    memset(&f1, 0, sizeof(f1));
    f1.next = &f1;
    f1.parent = &root;

    ts_element *cur = NULL;

    while (!feof(input))
    {
        char *l = fgets(line, sizeof(line), input);

        if (l != NULL && l[0] == '$')
        {
            char cmd[10];
            char args[100];
            int s = sscanf(line, "$ %s %s\n", cmd, args);

            if (s == 2)
            {
                if (strcmp("cd", cmd) == 0)
                {
                    if (strcmp("/", args) == 0)
                    {
                        cur = &f1;
                    }
                    else if (strcmp("..", args) == 0)
                    {
                        cur = cur->parent;
                    }
                    else
                    {
                        cur = gotToDir(args, cur);
                        assert(cur != NULL);
                        cur = cur->dir;
                    }
                }
                else if (strcmp("ls", cmd) == 0)
                {
                    
                }
                else
                {
                    fprintf(stderr, "Cmd unsupport [%s]\n", line);
                }
            }
            else if (s == 1)
            {
                if (strcmp("ls", cmd) == 0)
                {
                    
                }
                else
                {
                    fprintf(stderr, "Cmd unsupport [%s]\n", line);
                }
            }
            else
            {
                fprintf(stderr, "Erreur parse %d, [%s]\n", s, line);
            }
        }
        else
        {
            assert(cur != NULL);
            char type[20];
            int s = sscanf(line, "%s %s\n", type, cur->name);

            if (strcmp("dir", type) == 0)
            {
                cur->dir = malloc(sizeof(*cur->dir));
                assert(cur->dir != NULL);
                memset(cur->dir, 0, sizeof(*cur->dir));
                cur->dir->parent = cur;
                cur->dir->next = cur->dir;
            }
            else
            {
                int sn = sscanf(type, "%d", &cur->size);
                assert(sn == 1);
            }

            ts_element *e = malloc(sizeof(*e));
            assert(e != NULL);
            memset(e, 0, sizeof(*e));;
            e->parent = cur->parent;
            e->next = cur->next;
            cur->next = e;
            cur = cur->next;
        }
    }

    root.size = doCompute(&root);

    fprintf(stdout, "Total Space Used ; %d\n", root.size);
    fprintf(stdout, "need for update %d\n", need + root.size - disk);

    wStage1 = doComputeS1(&root, need + root.size - disk);

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "found marqueur at : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
