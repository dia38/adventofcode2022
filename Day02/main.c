#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

typedef enum 
{
    None        = 0,
    Rock        = 1,
    Paper       = 2,
    Scissors    = 3,
    Max         = 4,
} te_sym;

typedef enum
{
    Res_None    = 0,
    Lose        = 1,
    Nul         = 2,
    Win         = 3,
    Res_Max     = 4,
} te_res;

const char *mSymToStr[Max] =
{
    [None]      = "Error",
    [Rock]      = "Rock",
    [Paper]     = "Paper",
    [Scissors]  = "Scissors",
};

const char *mResToStr[Res_Max] = 
{
    [Res_None]  = "Error",
    [Lose]      = "Lose",
    [Nul]       = "Null",
    [Win]       = "Win",
};

typedef struct
{
    te_sym      col1;
    te_sym      col2;
    te_res      res;
    uint32_t    point;
} ts_game;


uint32_t doCompute(te_sym par1, te_sym par2)
{
    uint32_t res = 0;

    if (par1 == par2)
    {
        res = 3 + par2;

    }
    else if ((par1 == Rock) && (par2 == Paper))
    {
        res = 6 + par2;
    }
    else if ((par1 == Paper) && (par2 == Scissors))
    {
        res = 6 + par2;
    }
    else if ((par1 == Scissors) && (par2 == Rock))
    {
        res = 6 + par2;
    }
    else
    {
        res = par2;
    }

    return res;
}

uint32_t doCompute2(te_sym par1, te_res par2)
{
    uint32_t res = 0;

    switch (par2)
    {
        case Lose:
        {
            res = 0;
            switch (par1)
            {
                case Rock:
                {
                    res += Scissors;
                }
                break;

                case Paper:
                {
                    res += Rock;
                }
                break;

                case Scissors:
                {
                    res += Paper;
                }
                break;

                default:
                    break;
            }
        }
        break;
    
        case Nul:
        {
            res = 3 + par1;
        }   
        break;

        case Win:
        {
            res = 6;
            switch (par1)
            {
                case Rock:
                {
                    res += Paper;
                }
                break;

                case Paper:
                {
                    res += Scissors;
                }
                break;

                case Scissors:
                {
                    res += Rock;
                }
                break;

                default:
                    break;
            }
        }
        break;

        default:
        {

        }
        break;
    }

    return res;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wRes    = 0;
    uint32_t    wStage2 = 0;

    while (!feof(input))
    {
        ts_game cur;
        char col1, col2;

        int r = fscanf(input, "%c %c\n", &col1, &col2);
        
        if (r == 2)
        {
            switch (col1)
            {
                case 'A': cur.col1 = Rock; break;
                case 'B': cur.col1 = Paper; break;
                case 'C': cur.col1 = Scissors; break;
                default: cur.col1 = None; break;
            }
            switch (col2)
            {
                case 'X': {
                    cur.col2    = Rock; 
                    cur.res     = Lose;
                } break;
                case 'Y': {
                    cur.col2    = Paper;
                    cur.res     = Nul;
                } break;
                case 'Z': {
                    cur.col2    = Scissors;
                    cur.res     = Win;
                } break;
                default: {
                    cur.col2    = None;
                    cur.res     = None;
                } break;
            }

            cur.point = doCompute(cur.col1, cur.col2);
            wRes += cur.point;
            fprintf(stdout, "Stage 1 : %s vs %s ==> %" PRIu32 "\n", mSymToStr[cur.col1], mSymToStr[cur.col2], cur.point );
            
            cur.point = doCompute2(cur.col1, cur.res);
            wStage2 += cur.point;
            fprintf(stdout, "Stage 2 : %s ==> %s = %" PRIu32 "\n", mSymToStr[cur.col1], mResToStr[cur.res], cur.point );
        }
    }

    
    fprintf(stdout, "Stage 1 Total : %" PRIu32 "\n", wRes);
    fprintf(stdout, "Stage 2 Total : %" PRIu32 "\n", wStage2);


    return wRes;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
