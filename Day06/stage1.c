#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define STACk_SIZE 60
#define NB_STACK   9

typedef struct
{
    char    stack[STACk_SIZE];
    size_t  size;
} ts_stack;

static ts_stack boat[NB_STACK];

int32_t doCompute(void)
{
    int32_t wRes = 0;
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage1     = 0;
    size_t      idx         = 0;
    char        line[4];
    bool        stop        = false;

    memset(line, 0, sizeof(line));

    while (!feof(input) && idx < 4)
    {
        char c = fgetc(input);

        void *found = memchr(line, c, idx);

        while (found != NULL)
        {
            char tmp[4];
            memset(tmp, 0, sizeof(tmp));
            memcpy(tmp, line + 1, idx);
            memcpy(line, tmp, sizeof(line));
            idx--;
            found = memchr(line, c, idx);
        }

        line[idx] = c;
        idx++;

        wStage1++;
    }

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "found marqueur at : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
