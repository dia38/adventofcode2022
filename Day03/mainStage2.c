#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>

typedef struct
{
    char *cont;
    size_t len;
} ts_badge;

uint32_t doCompute(ts_badge parGroup[3])
{
    uint32_t wRes = 0;

    for (size_t i = 0; i < parGroup[0].len; i++)
    {
        char ch = parGroup[0].cont[i];

        if (isalpha(ch))
        {
            char *c = memchr(parGroup[0].cont, ch, parGroup[0].len);
            for (size_t j = 1; (j < 3) && (c != NULL); j++)
            {
                c = memchr(parGroup[j].cont, ch, parGroup[j].len);
            }

            if (c == NULL)
            {
                /* continue */
            }
            else if (isupper(*c))
            {
                fprintf(stdout, "share item type '%c' ==> %" PRIu32 "\n", ch,  (*c - 'A' + 27));
                wRes += *c - 'A' + 27;
            }
            else if (islower(*c))
            {
                fprintf(stdout, "share item type '%c' ==> %" PRIu32 "\n", ch,  (*c - 'a' + 1));
                wRes += *c - 'a' + 1;
            }

            c = memchr(parGroup[0].cont, ch, parGroup[0].len);
            while (c != NULL)
            {
                *c = ' ';
                c = memchr(parGroup[0].cont, ch, parGroup[0].len);
            }
        }
    }
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    char line[3][64];
    ts_badge group[3];
    uint32_t    wStage1 = 0;
    uint32_t    wStage2 = 0;

    while (!feof(input))
    {
        memset(group, 0, sizeof(group));
        memset(line, 0, sizeof(line));

        for (size_t i = 0; i < 3; i++)
        {
            int r = fscanf(input, "%s\n", line[i]);
            if (r == 1)
            {
                group[i].cont   = line[i];
                group[i].len    = strnlen(line[i], sizeof(line[0]));
            }
        }

        uint32_t r = doCompute(group);

        fprintf(stdout, "Total share item : %" PRIu32 "\n", r);
        wStage1 += r;

    }

    fprintf(stdout, "Total Share item : %" PRIu32 "\n", wStage1);

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
