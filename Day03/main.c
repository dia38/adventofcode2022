#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>


uint32_t doCompute(char *parStr, size_t parLen)
{
    const size_t len = parLen/2;
    char *comp1 = parStr;
    char *comp2 = parStr + len;
    uint32_t wRes = 0;

    for (size_t i = 0; i < len; i++)
    {
        char ch = comp1[i];

        if (isalpha(ch))
        {
            char *c = memchr(comp2, ch, len);
            if (c == NULL)
            {
                /* continue */
            }
            else if (isupper(*c))
            {
                fprintf(stdout, "share item type '%c' ==> %" PRIu32 "\n", ch,  *c - 'A' + 27);
                wRes += *c - 'A' + 27;
            }
            else if (islower(*c))
            {
                fprintf(stdout, "share item type '%c' ==> %" PRIu32 "\n", ch,  *c - 'a' + 1);
                wRes += *c - 'a' + 1;
            }

            c = memchr(comp1, ch, len);
            while (c != NULL)
            {
                *c = ' ';
                c = memchr(comp1, ch, len);
            }
        }
        
    }
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    char line[64];
    uint32_t    wStage1 = 0;
    uint32_t    wStage2 = 0;

    while (!feof(input))
    {
        int r = fscanf(input, "%s\n", line);
        
        if (r == 1)
        {
            size_t l = strnlen(line, sizeof(line));

            uint32_t r = doCompute(line, l);

            fprintf(stdout, "Total share item : %" PRIu32 "\n", r);
            wStage1 += r;
        }
    }

    fprintf(stdout, "Total Share item : %" PRIu32 "\n", wStage1);

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
