#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define STACk_SIZE 60
#define NB_STACK   9

typedef struct
{
    char    stack[STACk_SIZE];
    size_t  size;
} ts_stack;

static ts_stack boat[NB_STACK];

int32_t doCompute(void)
{
    int32_t wRes = 0;
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage1 = 0;
    char line[128];
    bool stop = false;

    memset(boat, 0, sizeof(boat));

    while (!feof(input) && !stop)
    {
        char *l = fgets(line, sizeof(line), input);
        const size_t len = strlen(l);

        for (int i = 1 ; i < len; i += 4)
        {
            ts_stack *tmp = &boat[ i / 4];
            if ( isalpha(l[i]))
            {
                tmp->stack[STACk_SIZE -1 - tmp->size++] = l[i];
            }
            else if (isdigit(l[i]))
            {
                stop = true;
            }
            else
            {
//                tmp->stack[16 - tmp->size++] = l[i];
            }
        }
    }

    // compact
    for (int i = 0 ; i < NB_STACK; i++)
    {
        ts_stack *tmp = &boat[ i ];
        size_t dst = 0;
        for (size_t j = 0; j < STACk_SIZE ; ++j)
        {
            if ((tmp->stack[j] != 0) && (j != dst))
            {
                tmp->stack[dst++] = tmp->stack[j];
                tmp->stack[j] = 0;
            }
        }
    }
    fgets(line, sizeof(line), input);

    while (!feof(input))
    {
        uint8_t nb;
        uint8_t colsrc;
        uint8_t coldst;
        int s = fscanf(input, "move %" SCNu8 " from %" SCNu8 " to %" SCNu8 "\n", 
                        &nb, &colsrc, &coldst  );
        coldst --;
        colsrc --;

        for (size_t i = 0; (i < nb) && (s == 3); i++)
        {
            if (boat[colsrc].size == 0)
            {
                fprintf(stdout, "Col src empty rest %" PRId32 "\n", nb - i );
            }
            else if (boat[coldst].size >=  STACk_SIZE)
            {
                fprintf(stdout, "Col dst full rest %" PRId32 "\n", nb - i );
            }
            else
            {
                boat[colsrc].size--;

                boat[coldst].stack[boat[coldst].size]
                = boat[colsrc].stack[boat[colsrc].size];
                
                boat[colsrc].stack[boat[colsrc].size] = 0;

                boat[coldst].size++;
            }
        }
    }

    fprintf(stdout, "TOP Stack  \n");
    for (size_t i = 0; i < NB_STACK; i++)
    {
        if (boat[i].size > 0)
        {
            fprintf(stdout, "[%c] ", boat[i].stack[boat[i].size - 1]);
        }
        else
        {
            fprintf(stdout, "[ ] ");
        }
    }
    
    fprintf(stdout, "\n");

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
