#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

typedef struct 
{
    uint8_t min;
    uint8_t max;
} ts_Sector;


int32_t doCompute(ts_Sector * parE1, ts_Sector * parE2)
{
    int32_t wRes = 0;

    if ((parE1->min >= parE2->min) && (parE1->max <= parE2->max))
    {
        wRes = -1;
    }
    else if ((parE2->min >= parE1->min) && (parE2->max <= parE1->max))
    {
        wRes = 1;
    }
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage1 = 0;

    ts_Sector elfe1;
    ts_Sector elfe2;

    while (!feof(input))
    {
        int r = fscanf(input, 
            "%" SCNu8 "-%" SCNu8 
            ",%" SCNu8 "-%" SCNu8 "\n", 
            &elfe1.min, &elfe1.max,
            &elfe2.min, &elfe2.max
        );
        
        if (r == 4)
        {
            int32_t r = doCompute(&elfe1, &elfe2);

            if (r < 0)
            {
                fprintf(stdout, "E1[%" PRIu8 " E2[%" PRIu8 "-%" PRIu8  "] %" PRIu8 "]\n",
                    elfe1.min, elfe2.min,
                    elfe2.max, elfe1.max
                );
                wStage1 ++;
            }
            else if (r > 0)
            {
                fprintf(stdout, "E2[%" PRIu8 " E1[%" PRIu8 "-%" PRIu8  "] %" PRIu8 "]\n",
                    elfe2.min, elfe1.min,
                    elfe1.max, elfe2.max
                );
                wStage1 ++;
            }
            else
            {
                fprintf(stdout, "E1[%" PRIu8 " - %" PRIu8 "] E2[%" PRIu8  " - %" PRIu8 "]\n",
                    elfe1.min, elfe1.max,
                    elfe2.min, elfe2.max
                );
            }
        }
    }

    fprintf(stdout, "Total Share item : %" PRIu32 "\n", wStage1);

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
